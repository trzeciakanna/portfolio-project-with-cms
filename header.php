<!DOCTYPE html>
<html lang="pl">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
      <link rel="stylesheet" href="/style/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="/slick/slick/slick.css" />
      <link rel="stylesheet" type="text/css" href="/slick/slick/slick-theme.css" />
      <script type="text/javascript" src="/tinymce/tinymce.min.js"></script>
      <link rel="stylesheet" href="/style/style.css">

      <title>John Smith - Web Designer</title>
  </head>
  <body class="d-flex justify-content-beetwen flex-column"<?php echo ($_SERVER['REQUEST_URI']=='/') ? 'data-spy="scroll" data-target=".navbar" data-offset="100"': '' ?>>
